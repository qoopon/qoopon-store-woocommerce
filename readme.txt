=== Qoopon Store ===
Contributors: tyt2y3
Requires at least: 4.9
Tested up to: 5.2
Stable tag: master
Requires PHP: 5.6
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

WooCommerce integration with Qoopon

== Description ==

This plugin allows you to connect your store to Qoopon, with additional features:

1. It enhances standard WooCommerce coupons by enriching them with thumbnails and unique Qoopon links
1. It also provides extra discount options like Buy One Get One Free and Group Product Discounts
1. It also allow customers to change product variations in shopping cart directly

Most importantly, you will be able to promote and distribute your coupon on the Qoopon network!

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Click 'Connect' in the admin notice to connect your store to Qoopon

== Frequently Asked Questions ==

= Where can I get support? =

help.qoopon.me

== Screenshots ==

1. abc

== Changelog ==

= 0.5.6 =
* Tweaks

= 0.5.5 =
* Support Qoopon Store Platform

= 0.5.4 =
* Added Chat Bubble
* Fixed coupon edit interface

= 0.5.3 =
* More detail coupon description
* Fixed add to cart quantity

= 0.5.2 =
* Automatic update

= 0.5.1 =
* Fixed bug on standard discount coupon

= 0.5.0 =
* Initial release


== Upgrade Notice ==

= 0.5.0 =
* Initial release
