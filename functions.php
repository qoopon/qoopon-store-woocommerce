<?php

/**
 * Plugin Name: Qoopon Store
 * Plugin URI: https://qoopon.me/qoopon-store
 * Description: Visualized, feature-rich coupon. Integrates with Qoopon platform.
 * Author: Qoopon
 * Author URI: https://qoopon.me/about
 *
 * Version: 0.5.6
 *
 * Requires at least: 4.9
 * Tested up to: 5.2
 *
 * WC requires at least: 3.3
 * WC tested up to: 3.6
 *
 */

defined( 'ABSPATH' ) || exit();

define('QOOPON_STORE_PLATFORM', basename(plugin_dir_path(__FILE__)) == 'qoopon-store-platform');

if (QOOPON_STORE_PLATFORM) {
    require_once plugin_dir_path(__FILE__) . 'platform/platform.php';
    register_activation_hook(__FILE__, array('Qoopon_Store_Platform', 'activation_hook'));

    require_once plugin_dir_path(__FILE__) . 'aliyun-oss/functions.php';
} else {
    require_once plugin_dir_path(__FILE__) . 'plugin-update-checker/plugin-update-checker.php';

    $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
        'https://bitbucket.org/qoopon/qoopon-store-woocommerce',
        __FILE__,
        'qoopon-store-woocommerce'
    );
}

require_once plugin_dir_path(__FILE__) . 'qoopon-store/qoopon-store.php';
if (!QOOPON_STORE_PLATFORM) {
    register_activation_hook(__FILE__, array('Qoopon_Store', 'activation_hook'));
}

define('CHT_FILE', __FILE__); // this file
require_once plugin_dir_path(__FILE__) . 'chaty/cht-icons.php';
