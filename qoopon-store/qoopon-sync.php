<?php

defined( 'ABSPATH' ) || exit();

Qoopon_Sync::get_instance();

class Qoopon_Sync
{
    const TABLE_VER = '1';
    const TABLE_NAME = 'qoopon_sync'; // MySQL table
    const LOG_LIMIT = 1000000;
    const ACTION_CREATE = 1;
    const ACTION_UPDATE = 2;
    const ACTION_TRASH = 3;
    const ACTION_DELETE = 4;

    private static $instance = null;
    private $post_types;
    private $post_types_sql;

    function __construct()
    {
        add_action( 'save_post_product', array($this, 'on_product_save'), 100, 3 );
        add_action( 'save_post_product_variation', array($this, 'on_product_variation_save'), 100, 3 );
        add_action( 'save_post_shop_coupon', array($this, 'on_coupon_save'), 100, 3 );
        add_action( 'woocommerce_new_order', array($this, 'on_new_order'), 99, 1 );
        add_action( 'woocommerce_saved_order_items', array($this, 'on_order_save'), 100, 2 );
        add_action( 'trashed_post', array($this, 'on_trash_post'), 101, 1 );
        add_action( 'delete_post', array($this, 'on_delete_post'), 102, 1 );
        add_action( 'rest_api_init', array($this, 'rest_api_init') );
        add_action( 'admin_notices', array($this, 'admin_notices') );
        add_action( 'wp_ajax_qoopon_connect_store', array($this, 'connect_store') );
    }

    public static function get_post_types()
    {
        return array('product', 'product_variation', 'shop_order', 'shop_coupon');
    }

    public static function get_post_types_sql()
    {
        return str_replace(']', ')', str_replace('[', '(', str_replace('"', "'", json_encode(self::get_post_types()))));
    }

    public static function get_instance()
    {
        if ( null === self::$instance ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function activation_hook()
    {
        global $wpdb;
        $db_ver = get_option('qoopon_sync_table_version');
        $table_name = $wpdb->prefix . self::TABLE_NAME;
        if ($db_ver != self::TABLE_VER ||
            $wpdb->get_var("show tables like '$table_name'") != $table_name) {
            self::create_table();
        }
        self::init_log_table();
    }

    public static function create_table()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . self::TABLE_NAME;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
          id bigint(20) unsigned AUTO_INCREMENT,
          post_type varchar(20) NOT NULL,
          post_id bigint(20) unsigned NOT NULL,
          action tinyint(1) NOT NULL,
          timestamp timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
          PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
        update_option( 'qoopon_sync_table_version', self::TABLE_VER );
    }

    public static function init_log_table()
    {
        global $wpdb;
        $log_table = $wpdb->prefix . self::TABLE_NAME;
        $ACTION_UPDATE = self::ACTION_UPDATE;
        $since = $wpdb->get_var(
            "select convert_tz(timestamp, @@session.time_zone, '+00:00')
            from $log_table order by id desc limit 1"
        );
        $andwhere = "";
        if ($since) {
            $andwhere = "and post_modified_gmt > '$since'";
        }
        $post_types_sql = self::get_post_types_sql();
        $wpdb->query(
            "insert into $log_table
            select
                null as id,
                post_type,
                id as post_id,
                $ACTION_UPDATE as action,
                convert_tz(`post_modified_gmt`, '+00:00', @@session.time_zone) as timestamp
            from {$wpdb->posts} posts
            where post_type in $post_types_sql
            $andwhere
            order by posts.id asc"
        );
    }

    public function on_product_save( $post_id, $post, $update )
    {
        self::on_post_save( $post_id, 'product', !$update ? SELF::ACTION_CREATE : SELF::ACTION_UPDATE );
    }

    public function on_product_variation_save( $post_id, $post, $update )
    {
        self::on_post_save( $post_id, 'product_variation', !$update ? SELF::ACTION_CREATE : SELF::ACTION_UPDATE );
    }

    public function on_coupon_save( $post_id, $post, $update )
    {
        self::on_post_save( $post_id, 'shop_coupon', !$update ? SELF::ACTION_CREATE : SELF::ACTION_UPDATE );
    }

    public function on_new_order( $post_id )
    {
        self::on_post_save( $post_id, 'shop_order', SELF::ACTION_CREATE );
    }

    public function on_order_save( $post_id, $order_items )
    {
        self::on_post_save( $post_id, 'shop_order', SELF::ACTION_UPDATE );
    }

    public function on_trash_post( $post_id )
    {
        $post_type = get_post_type( $post_id );
        if (in_array( $post_type, self::get_post_types(), true )) {
            self::on_post_save( $post_id, $post_type, SELF::ACTION_TRASH );
        }
    }

    public function on_delete_post( $post_id )
    {
        $post_type = get_post_type( $post_id );
        if (in_array( $post_type, self::get_post_types(), true )) {
            self::on_post_save( $post_id, $post_type, SELF::ACTION_DELETE );
        }
    }

    public static function on_post_save( $post_id, $post_type, $action )
    {
        switch ($post_type)
        {
            case 'shop_order':
            case 'shop_coupon':
                $pulse = true;
            break;
            default:
                $pulse = rand(0, 9) == 0;
        }
        if ($pulse && ($store_key = get_option('qoopon_sync_store_key'))) {
            wp_remote_post( Qoopon_Store::MY_QOOPON_URL.'/connect/store/pulse', array(
                    'method' => 'POST',
                    'timeout' => 5,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking' => false,
                    'headers' => array(),
                    'body' => array(
                        'store_key' => $store_key,
                    ),
                    'cookies' => array()
                )
            );
        }

        global $wpdb;
        $log_table = $wpdb->prefix . self::TABLE_NAME;
        $wpdb->insert( 
            $log_table, 
            array( 
                'post_type' => $post_type, 
                'post_id' => $post_id, 
                'action' => $action, 
            ), 
            array( 
                '%s', 
                '%d', 
                '%d',
            ) 
        );
        $oldest = intval($wpdb->get_var(
            "select id from $log_table order by id asc limit 1"
        ));
        $latest = intval($wpdb->get_var(
            "select id from $log_table order by id desc limit 1"
        ));
        if ($oldest <= $latest - self::LOG_LIMIT) {
            $wpdb->query(
                "delete from $log_table
                where ".$wpdb->prepare('id <= %d', $latest - self::LOG_LIMIT)
            );
        }
    }

    public function rest_api_init()
    {
        // API url: www.xxx.com/wp-json/wc-qoopon-sync/v1/ping
        register_rest_route( 'wc-qoopon-sync/v1', '/ping', array(
            'methods' => 'GET',
            'callback' => function () {
                update_option( 'qoopon_sync_wcapi_key', null );
                $logo = Qoopon_Store::get_store_logo();
                if ( function_exists( 'wc_get_base_location' ) ) {
                    $location = wc_get_base_location();
                    $country = $location['country'];
                    $state = $location['state'];
                } else {
                    $country_string = apply_filters( 'woocommerce_get_base_location', get_option( 'woocommerce_default_country' ) );
                    if ( strstr( $country_string, ':' ) ) {
                        list( $country, $state ) = explode( ':', $country_string );
                    } else {
                        $country = $country_string;
                        $state   = '';
                    }
                }
                return array(
                    'success' => true,
                    'name' => get_bloginfo('name'),
                    'description' => get_bloginfo('description'),
                    'icon' => $logo ?: get_site_icon_url(),
                    'country' => $country,
                    'state' => $state,
                    'city' => get_option('woocommerce_store_city'),
                    'address' => get_option('woocommerce_store_address') ?
                                get_option('woocommerce_store_address').', '.get_option('woocommerce_store_address_2') : 
                                get_option('woocommerce_store_address_2'),
                    'sell_to' => get_option('woocommerce_allowed_countries'),
                    'sell_to_these' => get_option('woocommerce_specific_allowed_countries'),
                    'sell_to_except' => get_option('woocommerce_all_except_countries'),
                    'currency' => function_exists( 'get_woocommerce_currency' ) ? get_woocommerce_currency() : get_option('woocommerce_currency'),
                );
            },
            'args' => array(
                'auth_key' => array(
                    'required' => true,
                ),
            ),
            'permission_callback' => array($this, 'qoopon_sync_auth'),
        ) );

        register_rest_route( 'wc-qoopon-sync/v1', '/disconnect', array(
            'methods' => 'GET',
            'callback' => function () {
                update_option( 'qoopon_sync_store_key', null );
                update_option( 'qoopon_sync_wcapi_key', null );
                return array(
                    'success' => true,
                );
            },
            'args' => array(
                'auth_key' => array(
                    'required' => true,
                ),
            ),
            'permission_callback' => array($this, 'qoopon_sync_auth'),
        ) );

        register_rest_route( 'wc-qoopon-sync/v1', '/logs', array(
            'methods' => 'GET',
            'callback' => function ( $request ) {
                global $wpdb;
                $table = $wpdb->prefix.self::TABLE_NAME;

                if ( $request['from'] ) {
                    $where = 'WHERE';
                    $and = 'AND';
                    $since = $wpdb->prepare('s1.id > %d', $request['from']);
                } else if ( $request['since'] ) {
                    $where = 'WHERE';
                    $and = 'AND';
                    $since = $wpdb->prepare('s1.timestamp > %s', $request['since']);
                }
                if ( $request['limit'] ) {
                    $limit = $wpdb->prepare('LIMIT %d', $request['limit']);
                }
                if ( $request['digest'] ) {
                    // only get latest of each post id
                    return $wpdb->get_results( "
                        SELECT s1.*
                        FROM $table s1
                        LEFT OUTER JOIN $table s2
                        ON (s1.post_id = s2.post_id AND s1.id < s2.id)
                        WHERE s2.id is NULL $and $since
                        ORDER BY s1.id
                        $limit
                    " );
                } else {
                    return $wpdb->get_results( "
                        SELECT * FROM $table s1
                        $where $since
                        ORDER BY s1.id
                        $limit
                    " );
                }
            },
            'args' => array(
                'auth_key' => array(
                    'required' => true,
                ),
                'digest' => array(
                    'required' => false,
                ),
                'since' => array(
                    'required' => false,
                    'validate_callback' => array($this, 'datetime_validator'),
                ),
                'from' => array(
                    'required' => false,
                    'validate_callback' => 'is_numeric',
                ),
                'limit' => array(
                    'required' => false,
                    'validate_callback' => 'is_numeric',
                ),
            ),
            'permission_callback' => array($this, 'qoopon_sync_auth'),
        ) );

        register_rest_route( 'wc-qoopon-sync/v1', '/post-status', array(
            'methods' => 'GET',
            'callback' => function ( $request ) {
                return get_post_status( $request['post_id'] );
            },
            'args' => array(
                'auth_key' => array(
                    'required' => true,
                ),
                'post_id' => array(
                    'required' => true,
                    'validate_callback' => 'is_numeric',
                ),
            ),
            'permission_callback' => array($this, 'qoopon_sync_auth'),
        ) );

        register_rest_route( 'wc-qoopon-sync/v1', '/list', array(
            'methods' => 'GET',
            'callback' => function ( $request ) {
                global $wpdb;
                if ( $request['post_type'] ) {
                    $post_type_where = $wpdb->prepare('post_type = %s', $request['post_type']);
                } else {
                    $post_type_where = "post_type in ".self::get_post_types_sql();
                }
                if ( $request['modified_since'] ) {
                    $and = 'and';
                    $modified_since_where = $wpdb->prepare('post_modified_gmt > %s', $request['modified_since']);
                }
                return $wpdb->get_results(
                    "select id, post_type, post_modified_gmt from {$wpdb->posts}
                    where $post_type_where $and $modified_since_where
                    order by post_type asc, post_modified_gmt asc"
                );
            },
            'args' => array(
                'auth_key' => array(
                    'required' => true,
                ),
                'post_type' => array(
                    'required' => false,
                    'validate_callback' => function($param, $request, $key) {
                        return in_array( $param, self::get_post_types(), true );
                    },
                ),
                'modified_since' => array(
                    'required' => false,
                    'validate_callback' => array($this, 'datetime_validator'),
                ),
            ),
            'permission_callback' => array($this, 'qoopon_sync_auth'),
        ) );
    }

    public function qoopon_sync_auth()
    {
        return apply_filters( 'determine_current_user', false )
            && password_verify( get_option('qoopon_sync_store_key'), $_GET['auth_key'] );
    }

    public function datetime_validator($param, $request, $key)
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $param, new DateTimeZone('UTC'));
        return $date && DateTime::getLastErrors()["warning_count"] == 0 && DateTime::getLastErrors()["error_count"] == 0;
    }

    public function admin_notices()
    {
        if ( get_option( 'qoopon_sync_store_key' ) &&
            ! get_option( 'qoopon_sync_wcapi_key' ) &&
            ! ( isset($_GET['qoopon-reconnect']) ) ) {
            return;
        }
        if ( (  preg_match('/plugins\.php/i', $_SERVER['REQUEST_URI']) ||
                preg_match('/post_type=shop_/i', $_SERVER['REQUEST_URI']) ||
                preg_match('/page=wc-/i', $_SERVER['REQUEST_URI']) )
            ) {
            ?>
            <style>
            .qoopon-message {
                overflow: hidden;
                position: relative;
                border-left-color: #4ec1c4 !important;
            }
            .qoopon-message .button-primary {
                background: #4ec1c4;
                border-color: #469496;
                text-shadow: 0 -1px 1px #469496,1px 0 1px #469496,0 1px 1px #469496,-1px 0 1px #469496;
                display: inline-block;
            }
            </style>
            <div id="message" class="updated qoopon-message">
                <p>
                    <strong><?php esc_html_e( 'Qoopon Setup', 'qoopon' ); ?></strong> &#8211; <?php esc_html_e( 'Connect your store to Qoopon.me to begin the journey.', 'qoopon' ); ?>
                </p>
                <p class="submit">
                    <a id="qoopon-connect-store-btn" href="#" class="button-primary">
                        <?php esc_html_e( 'Connect', 'qoopon' ); ?>
                    </a>
                    <script>
                    jQuery('#qoopon-connect-store-btn').click(function() {
                        if (this.clicked) return;
                        this.clicked = true;
                        jQuery.ajax({
                            method : "post",
                            dataType : "json",
                            url : ajaxurl,
                            data : {
                                action: "qoopon_connect_store",
                                reconnect: <?= isset($_GET['qoopon-reconnect']) ? 'true':'false' ?>,
                                nonce: "<?= wp_create_nonce("qoopon_connect_store") ?>"
                            },
                            success: function(result) {
                                if (result.success) {
                                    location.href = result.url + '&backlink=' + location.href.replace('qoopon-reconnect=true', '');
                                } else {
                                    jQuery('<span>'+result.message+'</span>').appendTo(jQuery('.qoopon-message'));
                                }
                            },
                            error: function(a,b,c) {
                                jQuery('<span>'+b+': '+c+'</span>').appendTo(jQuery('.qoopon-message'));
                            }
                        });
                    });
                    </script>
                </p>
            </div>
            <?php
        }
    }

    public function connect_store()
    {
        if ( ! wp_verify_nonce( isset($_POST['nonce']) ? $_POST['nonce'] : null, 'qoopon_connect_store' ) ) {
            echo json_encode(array(
                "success" => false,
                "message" => __( 'not authenticated', 'qoopon' )
            ));
            exit;
        }
        if ( ! get_option( 'qoopon_sync_store_key' ) || $_POST['reconnect'] ) {
            if ( ! get_option( 'qoopon_sync_wcapi_key' ) ) {
                require_once( plugin_dir_path(__FILE__) . 'qoopon-auth.php' );
                $keys = (new Qoopon_Auth())->create_qoopon_keys();
            } else {
                $keys = get_option( 'qoopon_sync_wcapi_key' );
            }
            $response = wp_remote_post( Qoopon_Store::MY_QOOPON_URL.'/connect/store/register', array(
                    'method' => 'POST',
                    'timeout' => 10,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking' => true,
                    'headers' => array(),
                    'body' => array(
                        'url' => get_home_url(),
                        'type' => 'WooCommerce',
                        'name' => get_bloginfo('name'),
                        'description' => get_bloginfo('description'),
                        'icon' => get_site_icon_url(),
                        'client_key' => $keys['consumer_key'],
                        'secret_key' => $keys['consumer_secret'],
                    ),
                    'cookies' => array()
                )
            );

            if ( is_wp_error( $response ) ) {
                echo json_encode(array(
                    "success" => false,
                    "message" => $response->get_error_message(),
                ));
                exit;
            }
            $result = json_decode($response['body']);
            if (!$result->success) {
                echo json_encode(array(
                    "success" => false,
                    "message" => __( 'Failed to register with Qoopon', 'qoopon' ) .': '. __( $result->message, 'qoopon' ),
                ));
                exit;
            }
            update_option( 'qoopon_sync_store_key', $result->key );
        }
        echo json_encode(array(
            "success" => true,
            "url" => Qoopon_Store::MY_QOOPON_URL.'/connect/store/connect?key='.get_option( 'qoopon_sync_store_key' ),
            "message" => __( 'success', 'qoopon' ),
        ));
        exit;
    }
}
