<?php

defined( 'ABSPATH' ) || exit();

Update_Variations_In_Cart::get_instance();

class Update_Variations_In_Cart
{
    private static $instance = null;

    function __construct()
    {
        // add js & css
        add_action( 'woocommerce_after_cart_table', array($this, 'inject_scripts') );

        // add edit button on cart page
        add_filter( 'woocommerce_get_item_data', array($this, 'cart_product_attr'), 20, 2 );

        // update product
        add_action( 'wp_ajax_qoopon_update_product_in_cart', array($this, 'update_product_in_cart') );
        add_action( 'wp_ajax_nopriv_qoopon_update_product_in_cart', array($this, 'update_product_in_cart') );
    }

    public static function get_instance()
    {
        if ( null === self::$instance ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function cart_product_attr( $item_data, $cart_item )
    {
        if (!is_cart()) {
            return $item_data;
        }
        $product = wc_get_product( $cart_item['product_id'] );
        if ($product->get_type() != 'variable') {
            return $item_data;
        }
        $attributes = $product->get_variation_attributes();
        $attr_map = array();
        foreach ( $attributes as $attr => $value )
        {
            $attr_map[ wc_attribute_label($attr) ] = $attr;
        }
        foreach ( $item_data as $i => $item )
        {
            if ( isset( $attr_map[strtolower($item['key'])] ) ) {
                $attr_name = $attr_map[strtolower($item['key'])];
                $options = $attributes[ $attr_name ];
                $item_data[$i]['display'] = self::get_display_for_product_attr(
                    $cart_item, $product, $attr_name, $options, $item['value']
                );
                unset( $attr_map[strtolower($item['key'])] );
            }
        }
        foreach ( $attr_map as $attr => $attr_name )
        {
            $value = $cart_item['variation']['attribute_'.$attr_name];
            $options = $attributes[ $attr_name ];
            array_unshift( $item_data, array(
                'key'   => $attr,
                'value' => $value,
                'display' => self::get_display_for_product_attr(
                    $cart_item, $product, $attr_name, $options, $value
                )
            ) );
        }
        /*$item_data[] = array(
            'key'   => 'Hi',
            'value' => json_encode($cart_item).json_encode($attributes),
        ); //*/
        return $item_data;
    }

    public static function get_display_for_product_attr($cart_item, $product, $attr_name, $options, $value)
    {
        return '<span class="product-attr-item-value">'.$value.'</span>'
        .'&nbsp;<span class="edit-variation-in-cart-btn dashicons dashicons-edit">'
        .htmlspecialchars(self::dropdown_variation_attribute_options(array(
            'name' => "cart[{$cart_item['key']}][$attr_name]",
            'class' => 'edit-variation-in-cart-select',
            'options' => $options,
            'attribute' => $attr_name,
            'product' => $product,
            'selected' => $cart_item['variation']['attribute_'.$attr_name]
        )))
        .'</span>';
    }

    public static function dropdown_variation_attribute_options($args)
    {
        ob_start();
        wc_dropdown_variation_attribute_options($args);
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    public function inject_scripts()
    {
        $ajax_url = admin_url('admin-ajax.php');
        echo "<script>\n";
        echo <<<'JAVASCRIPT'
        (function ($) {
            var edit_variation_in_cart_state = {};

            $(document).on('click', '.edit-variation-in-cart-btn', function(){
                if (!$(this).siblings('select').length) {
                    $(this).siblings('span.product-attr-item-value').hide();
                    $($(this).text()).insertBefore(this);
                } else {
                    var select = $(this).siblings('select');
                    edit_variation_in_cart_state[ $(select).attr('name') ] = null;
                    select.remove();
                    $(this).siblings('span.product-attr-item-value').show();
                    refresh_update_cart_button();
                }
            });

            $('.woocommerce').on('change', '.edit-variation-in-cart-select', function(){
                edit_variation_in_cart_state[ $(this).attr('name') ] = $(this).val();
                refresh_update_cart_button();
            });

            function refresh_update_cart_button()
            {
                var count = 0; for (var attr in edit_variation_in_cart_state) { if (edit_variation_in_cart_state[attr]) { count++; } }
                $( '.woocommerce-cart-form :input[name="update_cart"]' ).prop( 'disabled', count == 0 );
            }

            // copied and adapted from https://github.com/woocommerce/woocommerce/blob/master/assets/js/frontend/cart.js
            $('.woocommerce').on('submit', '.woocommerce-cart-form', function( evt, is_me ) { // we must have a higher priority than the existing one
                if (is_me === 'is_me') {
                    return;
                }
                var is_blocked = function( $node ) {
                    return $node.is( '.processing' ) || $node.parents( '.processing' ).length;
                };

                var $submit  = $( document.activeElement ),
                    $clicked = $( ':input[type=submit][clicked=true]' ),
                    $form    = $( evt.currentTarget );

                // For submit events, currentTarget is form.
                // For keypress events, currentTarget is input.
                if ( ! $form.is( 'form' ) ) {
                    $form = $( evt.currentTarget ).parents( 'form' );
                }

                if ( 0 === $form.find( '.woocommerce-cart-form__contents' ).length ) {
                    return;
                }

                if ( is_blocked( $form ) ) {
                    return false;
                }

                var count = 0; for (var attr in edit_variation_in_cart_state) { if (edit_variation_in_cart_state[attr]) { count++; } }
                if ( $clicked.is( ':input[name="update_cart"]' ) && count ) {
                    var items_changed = {};
                    for (var vari in edit_variation_in_cart_state)
                    {
                        if (edit_variation_in_cart_state[vari]) {
                            var item = vari.split('[')[1].split(']')[0];
                            var attr = vari.split(']')[1].split('[')[1].split(']')[0];
                            if (!items_changed[item]) {
                                items_changed[item] = {};
                            }
                            items_changed[item][attr] = edit_variation_in_cart_state[vari];
                        }
                    }
                    evt.stopPropagation();
                    evt.preventDefault();
                    $.ajax({
                        method : "post",
                        dataType : "json",
                        url : "$ajax_url",
                        data : {
                            action: "qoopon_update_product_in_cart",
                            items_changed: items_changed,
                        },
                        success: function(result) {
                            if (result.success) {
                                edit_variation_in_cart_state = {};
                                $('.woocommerce-cart-form').trigger('submit', ['is_me']);
                            }
                        }
                    });
                }
            });

        }(jQuery));
JAVASCRIPT;
        echo "</script>\n";
        echo "<style>\n";
        echo <<<'CSS'
        .edit-variation-in-cart-btn {
            cursor: pointer;
            overflow: hidden;
        }
        .edit-variation-in-cart-btn:hover {
            color: green;
        }
CSS;
        echo "</style>\n";
        wp_enqueue_style( 'dashicons' );
    }

    public function update_product_in_cart()
    {
        foreach ( $_POST['items_changed'] as $cart_item_key => $variation )
        {
            $item = WC()->cart->get_cart_item($cart_item_key);
            /* sample item
            {
              "key": "ab5e0b00dd24da4f0357996aec81a1fd",
              "product_id": 70,
              "variation_id": 2500,
              "variation": {
                "attribute_pa_color": "orange",
                "attribute_pa_size": "l"
              },
              "quantity": 1,
              "data_hash": "73eeacacb22971a1fb9aea8433851b6c",
              "line_tax_data": {
                "subtotal": [],
                "total": []
              },
              "line_subtotal": 60,
              "line_subtotal_tax": 0,
              "line_total": 60,
              "line_tax": 0,
              "data": {
                "rp_wcdpd_in_cart": true
              }
            } */
            if ($item)
            {
                $new_var = array_merge(array(), $item['variation']);
                foreach ( $variation as $attr => $val )
                {
                    $new_var[ 'attribute_'.$attr ] = $val;
                }
                $data_store = WC_Data_Store::load( 'product' );
                $variation_id = $data_store->find_matching_product_variation( wc_get_product($item['product_id']), $new_var );
                WC()->cart->add_to_cart(
                    $item['product_id'], $item['quantity'], $variation_id, $new_var, $item
                );
                WC()->cart->remove_cart_item($cart_item_key);
            }
        }
        echo json_encode(array(
            "success" => true,
            "message" => 'success'
        ));
        exit;
    }
}
