<?php

defined( 'ABSPATH' ) || exit();

class Qoopon_Store
{
    const QOOPON_ME = 'qoopon.me';
    const QOOPON_URL = 'https://qoopon.me';
    const MY_QOOPON_URL = 'https://my.qoopon.me';

    public static function activation_hook()
    {
        Qoopon_Sync::activation_hook();
    }

    public static function get_store_logo()
    {
        $logo = null;
        if ( function_exists( 'unero_get_option' ) ) {
            $logo = unero_get_option( 'logo' );
        } else {
            $custom_logo_id = get_theme_mod( 'custom_logo' );
            if ($custom_logo_id) {
                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            }
        }
        return $logo;
    }
}

require_once plugin_dir_path(__FILE__) . 'qoopon-sync.php';

if (get_option('qoopon_sync_store_key')) {
    require_once plugin_dir_path(__FILE__) . 'qoopon-coupon.php';
    require_once plugin_dir_path(__FILE__) . '../wcdpd/wc-dynamic-pricing-and-discounts.php';
    require_once plugin_dir_path(__FILE__) . 'qoopon-update-variations-in-cart.php';
}
