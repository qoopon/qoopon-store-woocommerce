<?php

defined( 'ABSPATH' ) || exit();

class Qoopon_Auth extends WC_Auth
{
    const KEY_NAME = 'qoopon_sync'; // WooCommerce API key

    public function create_qoopon_keys()
    {
        if ( 'yes' !== get_option( 'woocommerce_api_enabled' ) ) {
            update_option( 'woocommerce_api_enabled', 'yes' );
        }
        self::remove_keys();
        $key = $this->create_keys( self::KEY_NAME, self::KEY_NAME, 'read_write');
        update_option( 'qoopon_sync_wcapi_key', $key );
        return $key;
    }

    private static function get_wc_api_key()
    {
        global $wpdb;
        $user = $wpdb->get_row(
            $wpdb->prepare(
                "
                SELECT key_id, user_id, permissions, consumer_key, consumer_secret, nonces
                FROM {$wpdb->prefix}woocommerce_api_keys
                WHERE description like %s
                ", self::KEY_NAME.'%'
            )
        );
        return $user;
    }

    private static function remove_keys()
    {
        global $wpdb;
        $wpdb->query(
            $wpdb->prepare(
                "
                DELETE FROM {$wpdb->prefix}woocommerce_api_keys
                WHERE description like %s
                ", self::KEY_NAME.'%'
            )
        );
    }
}
