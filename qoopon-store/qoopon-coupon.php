<?php

defined( 'ABSPATH' ) || exit();

Qoopon_Coupon::get_instance();

class Qoopon_Coupon
{
    const QOOPON_CODE = 'qoopon-code';
    const QOOPON_META = 'qoopon-meta';
    const AJAX_USE_QOOPON = 'use_qoopon';

    private static $instance = null;

    public $qoopon_frame;

    function __construct()
    {
        $this->qoopon_frame = Qoopon_Store::QOOPON_URL.'/static/qoopon-frame-front.png';

        add_action( 'init', array($this, 'wp_init') );
        add_action( 'add_meta_boxes_shop_coupon', array($this, 'add_meta_boxes') );
        add_action( 'woocommerce_coupon_options_save', array($this, 'woocommerce_coupon_options_save'), 10, 2 );
        add_action( 'wp_ajax_'.self::AJAX_USE_QOOPON, array($this, 'use_qoopon_member') );
        add_action( 'wp_ajax_nopriv_'.self::AJAX_USE_QOOPON, array($this, 'use_qoopon_public') );
        add_filter( 'woocommerce_cart_totals_coupon_label', array($this, 'render_qoopon_label'), 100, 2 );
        add_filter( 'woocommerce_cart_totals_coupon_html', array($this, 'render_qoopon_html'), 100, 3 );
        add_filter( 'woocommerce_get_item_data', array($this, 'render_cart_item'), 100, 2 );
        add_filter( 'woocommerce_coupon_error', array($this, 'woocommerce_coupon_error'), 5, 3 );
        add_filter( 'woocommerce_get_cart_item_from_session', array($this, 'get_cart_item_from_session'), 10, 2 );
        add_filter( 'woocommerce_coupon_discount_types', array($this, 'get_all_discount_types'), 10, 1 );
        add_action( 'woocommerce_cart_item_restored', array($this, 'woocommerce_cart_item_restored'), 10, 2 );
        add_action( 'woocommerce_checkout_create_order_line_item', array($this, 'checkout_create_order_line_item'), 10, 4 );
        add_filter( 'woocommerce_order_item_get_formatted_meta_data', array($this, 'render_order_item'), 10, 2 );
        add_filter( 'woocommerce_admin_order_item_coupon_url', array($this, 'order_item_coupon_url'), 10, 3 );
        add_action( 'woocommerce_admin_order_totals_after_discount', array($this, 'admin_order_totals'), 10, 1 );
    }

    public static function get_instance()
    {
        if ( null === self::$instance ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function wp_init()
    {
        add_post_type_support( 'shop_coupon', 'thumbnail' );
    }

    public function add_meta_boxes()
    {
        add_meta_box(
            'qoopon_coupon_config_box', // $id
            'Qoopon option', // $title
            array($this, 'qoopon_coupon_config_box'), // $callback
            'shop_coupon' // $screen
        );
    }

    public function qoopon_coupon_config_box()
    {
        global $post;
        //$option = get_option('rp_wcdpd_settings');
        $coupon = new WC_Coupon( $post->ID );
        $option = $coupon->get_meta(self::QOOPON_META, true);

        ?>
        <style>
        #qoopon_coupon_config_box.postbox .inside {
            margin: 0;
            padding: 0;
        }
        #qoopon_coupon_config_box .woocommerce_options_panel {
            float: left;
            width: 80%;
        }
        ul.wc-tabs.qoopon_data_tabs li.qoopon_general a::before {
            content: "\f513";
        }
        ul.wc-tabs.qoopon_data_tabs li.qoopon_sales a::before {
            content: "\f174";
        }
        .qoopon-preview {
            width: 100%;
            height: 600px;
        }
        #woocommerce-coupon-description {
            height: 180px;
        }
        </style>
        <div class="woocommerce"><div class="panel-wrap">
            <div class="wc-tabs-back"></div>

            <ul class="qoopon_data_tabs wc-tabs" style="display:none;">
                <?php
                $coupon_data_tabs = array(
                    'general' => array(
                        'label'  => __( 'General', 'qoopon' ),
                        'target' => 'qoopon_general',
                        'class'  => 'qoopon_general',
                    ),
                    'scheme' => array(
                        'label'  => __( 'Sales scheme', 'qoopon' ),
                        'target' => 'qoopon_sales',
                        'class'  => 'qoopon_sales',
                    ),
                );

                foreach ( $coupon_data_tabs as $key => $tab ) :
                    ?>
                    <li class="<?php echo $key; ?>_options <?php echo $key; ?>_tab <?php echo implode( ' ', (array) $tab['class'] ); ?>">
                        <a href="#<?php echo $tab['target']; ?>">
                            <span><?php echo esc_html( $tab['label'] ); ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div id="qoopon_general" class="panel woocommerce_options_panel">
                <?php
                if ( $qcode = get_post_meta( $post->ID, self::QOOPON_CODE, true ) ) {
                    $qoopon_url = Qoopon_Store::QOOPON_URL.'/'.$qcode;
                    $qoopon_edit_url = Qoopon_Store::QOOPON_URL.'/qcodes/edit/'.$qcode;
                    $qoopon_link = Qoopon_Store::QOOPON_ME.'/'.$qcode;
                    echo '<iframe class="qoopon-preview" src="'.$qoopon_edit_url.'"></iframe>';
                    echo "<p style='text-align:center;'>".__( 'Qoopon Link', 'qoopon' ).": <a href='$qoopon_url'>$qoopon_link</a></p>";
                }
                if ( isset($_GET['qoopon-debug']) ) {
                    echo '<textarea name="vpc-config-json" rows="12" cols="80">';
                        echo json_encode($option, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
                    echo '</textarea>';
                }
                ?>
                <?php
                woocommerce_wp_checkbox(
                    array(
                        'id'          => 'qoopon_auto_desc',
                        'label'       => __( 'Smart Description', 'qoopon' ),
                        'description' => sprintf( __( 'Generate coupon description automatically.', 'qoopon' ) ),
                        'desc_tip'    => true,
                        'value'       => wc_bool_to_string( self::get_meta_field( $option, 'auto_desc', in_array( $coupon->get_discount_type(), array_keys(self::get_qoopon_discount_types()) ) ) ),
                    )
                );
                ?>
            </div>
            <div id="qoopon_sales" class="panel woocommerce_options_panel">
                <?php
                woocommerce_wp_select(
                    array(
                        'id'      => 'qoopon_sales_scheme',
                        'label'   => __( 'Sales scheme', 'qoopon' ),
                        'options' => wc_get_coupon_types(),
                        'value'   => $coupon->get_discount_type(),
                    )
                );
                ?>

                <div class="sub_option_panel" data-option-for="basic">
                    <p><?= __( 'Please set the Coupon amount in the "Coupon data" panel', 'qoopon' ) ?></p>
                </div>

                <p class="form-field" data-option-not-for="basic">
                    <label><?php _e( 'Products', 'woocommerce' ); ?></label>
                    <select name="qoopon_coupon_product_ids[]" class="wc-product-search" multiple="multiple" style="width: 50%;" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'woocommerce' ); ?>" data-action="woocommerce_json_search_products_and_variations">
                        <?php
                        $product_ids = self::get_sale_product( $option );

                        foreach ( $product_ids as $product_id ) {
                            $product = wc_get_product( $product_id );
                            if ( is_object( $product ) ) {
                                echo '<option value="' . esc_attr( $product_id ) . '"' . selected( true, true, false ) . '>' . wp_kses_post( $product->get_formatted_name() ) . '</option>';
                            }
                        }
                        ?>
                    </select>
                    <?php echo wc_help_tip( __( 'Products promoted in the sales scheme.', 'qoopon' ) ); ?>
                </p>

                <div class="sub_option_panel" data-option-for="bogo">
                <?php
                    woocommerce_wp_text_input(
                        array(
                            'id'          => 'qoopon_bogo_quantity',
                            'label'       => __( 'Number of products (X)', 'qoopon' ),
                            'description' => __( 'Upon adding X number of selected products to cart, the X+1th item will be discounted.', 'qoopon' ),
                            'type'        => 'number',
                            'desc_tip'    => true,
                            'value'       => self::get_option_field( $option, 'bogo_purchase_quantity', 1 ),
                        )
                    );

                    woocommerce_wp_text_input(
                        array(
                            'id'          => 'qoopon_bogo_discount',
                            'label'       => __( 'Discount percentage (%)', 'qoopon' ),
                            'type'        => 'number',
                            'value'       => self::get_option_field( $option, 'bogo_pricing_value', 100 ),
                        )
                    );

                    woocommerce_wp_checkbox(
                        array(
                            'id'          => 'qoopon_bogo_repeat',
                            'label'       => __( 'Repeating', 'qoopon' ),
                            'description' => sprintf( __( 'If yes, then a "Buy 1 Get 1" scheme will also allow customers to "Buy 2 Get 2".', 'qoopon' ) ),
                            'value'       => wc_bool_to_string( self::get_bogo_repeat( $option ) ),
                        )
                    );

                    woocommerce_wp_checkbox(
                        array(
                            'id'          => 'qoopon_bogo_mixing',
                            'label'       => __( 'Mix & Match', 'qoopon' ),
                            'description' => sprintf( __( 'If yes, then customers can freely mix and match products selected above.', 'qoopon' ) ),
                            'value'       => wc_bool_to_string( self::get_bogo_mix( $option ) ),
                        )
                    );
                ?>
                </div>

                <div class="sub_option_panel" data-option-for="group">
                <?php
                    woocommerce_wp_text_input(
                        array(
                            'id'          => 'qoopon_group_quantity',
                            'label'       => __( 'Number of products (X)', 'qoopon' ),
                            'type'        => 'number',
                            'value'       => self::get_group_quantity( $option ),
                        )
                    );

                    woocommerce_wp_text_input(
                        array(
                            'id'          => 'qoopon_group_pricing',
                            'label'       => __( 'Price (Y)', 'qoopon' ),
                            'type'        => 'number',
                            'value'       => self::get_group_pricing( $option ),
                        )
                    );

                    woocommerce_wp_text_input(
                        array(
                            'id'          => 'qoopon_group_discount',
                            'label'       => __( 'Discount (Y)', 'qoopon' ),
                            'type'        => 'number',
                            'value'       => self::get_group_pricing( $option ),
                        )
                    );

                    woocommerce_wp_text_input(
                        array(
                            'id'          => 'qoopon_group_percent',
                            'label'       => __( 'Discount (%)', 'qoopon' ),
                            'type'        => 'number',
                            'value'       => self::get_group_pricing( $option ),
                        )
                    );
                ?>
                </div>

                <script>
                jQuery('#woocommerce-coupon-description').change(function() {
                    jQuery('#qoopon_auto_desc').prop('checked', false).removeAttr('checked');
                });
                jQuery('#discount_type, #qoopon_sales_scheme').change(function() {
                    jQuery('#discount_type, #qoopon_sales_scheme').val(jQuery(this).val());
                    if (!jQuery(this).is(jQuery('#qoopon_sales_scheme'))) {
                        qoopon_update_for_sales_scheme.call(jQuery('#qoopon_sales_scheme'));
                    }
                });
                jQuery('#qoopon_sales_scheme').each(function(i, el){
                    var $=jQuery;
                    $(el).find('option[value="bogo"], option[value="bogo_x"], option[value="bogo_xx"]').wrapAll(
                        '<optgroup label="<?= __( 'Buy One Get One', 'qoopon' ) ?>"/>'
                    );
                    $(el).find('option[value="group"], option[value="group_discount"], option[value="group_discount_percent"]').wrapAll(
                        '<optgroup label="<?= __( 'Buy 3 For $100', 'qoopon' ) ?>"/>'
                    );
                    $(el).change(qoopon_update_for_sales_scheme);
                    $(el).change();
                });
                function qoopon_update_for_sales_scheme()
                {
                    var $=jQuery;
                    if (['percent', 'fixed_cart', 'fixed_product'].indexOf($(this).val()) !== -1) {
                        $('#qoopon_coupon_config_box .sub_option_panel[data-option-for="basic"]').show();
                        $('#qoopon_coupon_config_box .form-field[data-option-not-for="basic"]').hide();
                    } else {
                        $('#qoopon_coupon_config_box .sub_option_panel[data-option-for="basic"]').hide();
                        $('#qoopon_coupon_config_box .form-field[data-option-not-for="basic"]').show();
                    }

                    if ($(this).val().indexOf('bogo') === 0) {
                        switch ($(this).val())
                        {
                            case 'bogo':
                                $('#qoopon_bogo_quantity').val(1).parent().hide();
                                $('#qoopon_bogo_discount').val(100).parent().hide();
                            break;
                            case 'bogo_x':
                                $('#qoopon_bogo_quantity').parent().show();
                                $('#qoopon_bogo_discount').val(100).parent().hide();
                            break;
                            case 'bogo_xx':
                                $('#qoopon_bogo_quantity').parent().show();
                                $('#qoopon_bogo_discount').parent().show();
                            break;
                        }
                        $('#qoopon_coupon_config_box .sub_option_panel[data-option-for="bogo"]').show();
                    } else {
                        $('#qoopon_coupon_config_box .sub_option_panel[data-option-for="bogo"]').hide();
                    }

                    if ($(this).val().indexOf('group') === 0) {
                        switch ($(this).val())
                        {
                            case 'group':
                                $('#qoopon_group_pricing').parent().show();
                                $('#qoopon_group_discount').parent().hide();
                                $('#qoopon_group_percent').parent().hide();
                            break;
                            case 'group_discount':
                                $('#qoopon_group_pricing').parent().hide();
                                $('#qoopon_group_discount').parent().show();
                                $('#qoopon_group_percent').parent().hide();
                            break;
                            case 'group_discount_percent':
                                $('#qoopon_group_pricing').parent().hide();
                                $('#qoopon_group_discount').parent().hide();
                                $('#qoopon_group_percent').parent().show();
                            break;
                        }
                        $('#qoopon_coupon_config_box .sub_option_panel[data-option-for="group"]').show();
                    } else {
                        $('#qoopon_coupon_config_box .sub_option_panel[data-option-for="group"]').hide();
                    }
                }
                </script>
            </div>
            <div class="clear"></div>
        </div></div>
        <?php
    }

    public function woocommerce_coupon_options_save( $post_id, $coupon )
    {
        $meta = array();
        if (isset( $_POST['product_ids'] ) ||
            isset( $_POST['qoopon_coupon_product_ids'] ) ) {
            if (isset( $_POST['qoopon_coupon_product_ids'] ) &&
                isset( $_POST['qoopon_sales_scheme'] )) {
                $coupon->set_product_ids( $_POST['qoopon_coupon_product_ids'] );
                $coupon->set_discount_type( $_POST['qoopon_sales_scheme'] );
            }
            $products = array();
            $product_variations = array();
            $all_products = (array) $_POST['qoopon_coupon_product_ids'];
            if (!$all_products) {
                $all_products = (array) $_POST['product_ids'];
            }
            foreach ($all_products as $prod_id)
            {
                switch ( get_post_type( $prod_id ) )
                {
                    case 'product':
                        $products[] = $prod_id;
                    break;
                    case 'product_variation':
                        $product_variations[] = $prod_id;
                    break;
                }
            }
            if (count($products) && count($product_variations)) {
                foreach ($product_variations as $prod_id)
                {
                    $products[] = wp_get_post_parent_id($prod_id);
                }
                $product_variations = array();
            }
            $description = explode("\n", $coupon->get_description())[0];

            if (strpos($_POST['qoopon_sales_scheme'], 'bogo') === 0) {

                $meta['product_pricing'] = array(
                    'uid' => self::get_id(),
                    'exclusivity' => 'all',
                    'note' => '',
                    'public_note' => $description,
                    'bogo_pricing_method' => 'discount__percentage',
                    'bogo_pricing_value' => absint( $_POST['qoopon_bogo_discount'] ),
                    'method' => isset( $_POST['qoopon_bogo_repeat'] ) ? 'bogo_xx_repeat':'bogo_xx',
                    'quantities_based_on' => isset( $_POST['qoopon_bogo_mixing'] ) ? 'cumulative__all':'individual__product',
                    'bogo_purchase_quantity' => absint( $_POST['qoopon_bogo_quantity'] ),
                    'bogo_receive_quantity' => 1,
                    'conditions' => array(),
                    'bogo_product_conditions' => array(),
                );
                if ($products) {
                    $meta['product_pricing']['conditions'][] = array(
                        'uid' => self::get_id(),
                        'type' => 'product__product',
                        'method_option' => 'in_list',
                        'products' => $products,
                    );
                }
                if ($product_variations) {
                    $meta['product_pricing']['conditions'][] = array(
                        'uid' => self::get_id(),
                        'type' => 'product__variation',
                        'method_option' => 'in_list',
                        'product_variations' => $product_variations,
                    );
                }

                if ( $meta['product_pricing']['bogo_pricing_value'] < 100 ) {
                    $coupon->set_discount_type( 'bogo_xx' );
                } else if ( $meta['product_pricing']['bogo_purchase_quantity'] > 1 ) {
                    $coupon->set_discount_type( 'bogo_x' );
                } else {
                    $coupon->set_discount_type( 'bogo' );
                }

            } else if (strpos($_POST['qoopon_sales_scheme'], 'group') === 0) {

                $meta['product_pricing'] = array(
                    'uid' => self::get_id(),
                    'exclusivity' => 'all',
                    'note' => '',
                    'public_note' => $description,
                    'group_pricing_method' => 
                        ($_POST['qoopon_sales_scheme'] === 'group' ? 'fixed__price_per_group' : 
                        ($_POST['qoopon_sales_scheme'] === 'group_discount' ? 'discount__amount_per_group' : 
                         'discount__percentage')),
                    'group_pricing_value' => 
                        ($_POST['qoopon_sales_scheme'] === 'group' ? absint( $_POST['qoopon_group_pricing'] ): 
                        ($_POST['qoopon_sales_scheme'] === 'group_discount' ? absint( $_POST['qoopon_group_discount'] ): 
                         absint( $_POST['qoopon_group_percent'] ))),
                    'method' => 'group_repeat',
                    'group_quantities_based_on' => 'group_all',
                    'group_products' => array(),
                    'conditions' => array(),
                );
                if ($products) {
                    $meta['product_pricing']['group_products'][] = array(
                        'uid' => self::get_id(),
                        'quantity' => absint( $_POST['qoopon_group_quantity'] ),
                        'type' => 'product__product',
                        'method_option' => 'in_list',
                        'products' => $products,
                    );
                }
                if ($product_variations) {
                    $meta['product_pricing']['group_products'][] = array(
                        'uid' => self::get_id(),
                        'quantity' => absint( $_POST['qoopon_group_quantity'] ),
                        'type' => 'product__variation',
                        'method_option' => 'in_list',
                        'product_variations' => $product_variations,
                    );
                }
            }
        }
        if ( $_POST['qoopon_auto_desc'] ) {
            $meta['auto_desc'] = true;
            $coupon->set_description(self::get_qoopon_summary($coupon, $meta));
        } else {
            $meta['auto_desc'] = false;
        }
        $coupon->save();
        $thumbnail = get_the_post_thumbnail_url( $post_id );
        if ($thumbnail === false && ($products || $product_variations)) {
            $prod_id = $products ? $products[0] : $product_variations[0];
            $thumbnail_id = get_post_thumbnail_id( $prod_id );
            set_post_thumbnail( $post_id, $thumbnail_id );
            $thumbnail = wp_get_attachment_image_url( $thumbnail_id, 'post-thumbnail' );
        }
        $meta['thumbnail'] = $thumbnail;

        update_post_meta($post_id, self::QOOPON_META, $meta);

        $text = str_replace("\n\n", "\n", str_replace("\r\n", "\n", $coupon->get_description()));
        $title = explode("\n", $text)[0];
        $summary = isset(explode("\n", $text)[1]) ? explode("\n", $text)[1] : '';

        if (!($qcode = get_post_meta( $post_id, self::QOOPON_CODE, true ))) {
            $response = wp_remote_post( Qoopon_Store::MY_QOOPON_URL.'/connect/qcode/create', array(
                    'method' => 'POST',
                    'timeout' => 10,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking' => true,
                    'headers' => array(),
                    'body' => array(
                        'store_key' => get_option('qoopon_sync_store_key'),
                        'url' => admin_url( 'admin-ajax.php' ) .'?action='. self::AJAX_USE_QOOPON,
                        'title' => $title,
                        'summary' => $summary,
                        'thumbnail' => $thumbnail,
                    ),
                    'cookies' => array()
                )
            );
            if (!is_wp_error( $response )) {
                $result = json_decode($response['body']);
                if ($result && $result->success) {
                    update_post_meta($post_id, self::QOOPON_CODE, $result->qcode);
                }
            }
        } else {
            $response = wp_remote_post( Qoopon_Store::MY_QOOPON_URL.'/connect/qcode/update', array(
                    'method' => 'POST',
                    'timeout' => 5,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking' => true,
                    'headers' => array(),
                    'body' => array(
                        'store_key' => get_option('qoopon_sync_store_key'),
                        'qcode' => $qcode,
                        'title' => $title,
                        'summary' => $summary,
                        'thumbnail' => $thumbnail,
                    ),
                    'cookies' => array()
                )
            );
        }
    }

    private static function get_sale_product( $option )
    {
        if ( $option && isset( $option['product_pricing'] ) ) {
            $rules = array_merge(
                self::get_sub_option_field($option['product_pricing'], 'group_products', array()),
                self::get_sub_option_field($option['product_pricing'], 'conditions', array())
            );
            $products = array();
            foreach ($rules as $rule)
            {
                if ( isset($rule['products']) ) {
                    $products = array_merge($products, $rule['products']);
                }
                if ( isset($rule['product_variations']) ) {
                    $products = array_merge($products, $rule['product_variations']);
                }
            }
            return $products;
        }
        return array();
    }

    private static function get_sales_scheme( $option, $default )
    {
        if ( $option && isset( $option['product_pricing'] ) ) {
            if ( isset( $option['product_pricing']['bogo_pricing_method'] ) ) {
                if ( $option['product_pricing']['bogo_pricing_value'] < 100 ) {
                    return 'bogo_xx';
                } else if ( $option['product_pricing']['bogo_purchase_quantity'] > 1 ) {
                    return 'bogo_x';
                } else {
                    return 'bogo';
                }
            }
            if ( isset( $option['product_pricing']['group_pricing_method'] ) ) {
                switch ( $option['product_pricing']['group_pricing_method'] )
                {
                    case 'fixed__price_per_group': return 'group';
                    case 'discount__amount_per_group': return 'group_discount';
                    case 'discount__percentage': return 'group_discount_percent';
                }
            }
        }
        return $default;
    }

    private static function get_bogo_repeat( $option )
    {
        $repeat = self::get_option_field( $option, 'method', 'bogo_xx_repeat' );
        if ($repeat === 'bogo_xx_repeat') {
            return true;
        }
    }

    private static function get_bogo_mix( $option )
    {
        $repeat = self::get_option_field( $option, 'quantities_based_on', 'individual__product' );
        if ($repeat === 'cumulative__all') {
            return true;
        }
    }

    private static function get_group_quantity( $option, $default = 3 )
    {
        $option = self::get_option_field( $option, 'group_products', $default );
        if (is_array($option)) {
            return self::get_sub_option_field( $option[0], 'quantity', $default );
        } else {
            return $option;
        }
    }

    private static function get_purchase_quantity( $option )
    {
        if ( $option && $option['product_pricing'] && 
             isset($option['product_pricing']['bogo_purchase_quantity']) ) {
            return $option['product_pricing']['bogo_purchase_quantity'] + 1;
        }
        return self::get_group_quantity( $option, 1 );
    }

    private static function get_group_pricing( $option )
    {
        return self::get_option_field( $option, 'group_pricing_value', 50 );
    }

    private static function get_meta_field( $option, $field, $default )
    {
        if ( $option &&
             isset($option[$field])) {
            return $option[$field];
        }
        return $default;
    }

    private static function get_option_field( $option, $field, $default )
    {
        if ( $option && $option['product_pricing'] && 
             isset($option['product_pricing'][$field])) {
            return $option['product_pricing'][$field];
        }
        return $default;
    }

    private static function get_sub_option_field( $option, $field, $default )
    {
        if ( $option && isset($option[$field]) ) {
            return $option[$field];
        }
        return $default;
    }

    private static function get_hash()
    {
        // reference wcdpd/rightpress/rightpress-helper.class.php
        $data = (rand() . time() . rand());
        $hash = md5($data);
        return substr($hash, 0, 8);
    }

    private static function get_id()
    {
        return 'rp_wcdpd_'.self::get_hash();
    }

    public function use_qoopon_member()
    {
        return $this->use_qoopon('member');
    }

    public function use_qoopon_public()
    {
        return $this->use_qoopon('public');
    }

    private function use_qoopon($user)
    {
        if (!isset($_GET['a']) || !isset($_GET['b']) ||
            !$_GET['a'] || !$_GET['b']) {
            exit('error');
        }
        if (!isset($_SERVER['HTTP_REFERER']) ||
            (strpos($_SERVER['HTTP_REFERER'], Qoopon_Store::QOOPON_URL) !== 0 &&
             strpos($_SERVER['HTTP_REFERER'], str_replace('https', 'http', Qoopon_Store::QOOPON_URL)) !== 0)) {
            exit('bad');
        }
        if ( $coupon_id = $this->qoopon_lookup($_GET['a']) ) {
            $coupon = new WC_Coupon( $coupon_id );
            $coupon_code = $coupon->get_code();
            $option = get_post_meta( $coupon_id, self::QOOPON_META, true );
            $product_ids = $coupon->get_product_ids();
            if ($product_ids && is_array($product_ids)) {
                $total_quantity = self::get_purchase_quantity( $option ) ?: 1;
                $remainder = $total_quantity % count($product_ids);
                $quotient = floor($total_quantity / count($product_ids));
                foreach ($product_ids as $i => $prod_id)
                {
                    if ((strpos($coupon->get_discount_type(), 'bogo') === 0 && self::get_bogo_repeat( $option )) ||
                        strpos($coupon->get_discount_type(), 'group') === 0) {
                        $qty = $quotient + ($i < $remainder ? 1 : 0);
                    } else {
                        $qty = $total_quantity;
                    }
                    $prod = wc_get_product( $prod_id );
                    $var_id = 0;
                    $var_attr = array();
                    if ($prod->get_type() === 'variable') {
                        $var = $prod->get_available_variations()[0];
                        $var_id = $var['variation_id'];
                        $var_attr = $var['attributes'];
                    } else if ($prod->get_type() === 'variation') {
                        $var_id = $prod_id;
                        $prod_id = wp_get_post_parent_id( $var_id );
                        $var_attr = $prod->get_variation_attributes();
                    }
                    if ($var_id) {
                        $options = wc_get_product( $prod_id )->get_variation_attributes();
                        // $var_attr = {"attribute_pa_color":"gold","attribute_pa_size":""}
                        // $options = {"pa_color":["orange","gold"],"pa_size":["l","m","s","xl","xxl"]}
                        // wc_add_notice( json_encode($var_attr).json_encode($options), 'error' );
                        foreach ($var_attr as $attr => $val)
                        {
                            if (!$val) {
                                $attre = str_replace('attribute_', '', $attr);
                                if ( isset( $options[$attre] ) && count( $options[$attre] ) > 0 ) {
                                    $var_attr[$attr] = $options[$attre][0];
                                }
                            }
                        }
                    }
                    WC()->cart->add_to_cart(
                        $prod_id, $qty, $var_id, $var_attr, array(
                            self::QOOPON_CODE => array(
                                'a' => $_GET['a'],
                                'b' => $_GET['b'],
                                'c' => $coupon_code
                            )
                        )
                    );
                }
                WC()->cart->apply_coupon( $coupon_code );
            } else {
                wc_add_notice( __( 'Sorry, but the Qoopon you got is not configured correctly.', 'qoopon' ), 'error' );
            }
        } else {
            wc_add_notice( __( 'Sorry, but the Qoopon you got is invalid.', 'qoopon' ), 'error' );
        }
        wp_redirect( get_permalink( wc_get_page_id( 'cart' ) ) );
        exit;
    }

    public function qoopon_lookup( $qoopon )
    {
        global $wpdb;
        $meta = $wpdb->get_row(
            $wpdb->prepare(
                "
                SELECT post_id
                FROM {$wpdb->postmeta}
                WHERE meta_key = %s AND meta_value = %s
                ", self::QOOPON_CODE, $qoopon
            )
        );
        if ($meta) {
            return $meta->post_id;
        }
        return false;
    }

    public function is_qoopon($code)
    {
        if (get_page_by_title( $code, OBJECT, 'shop_coupon' )) {
            $coupon = new WC_Coupon( $code );
            return $coupon->get_meta(self::QOOPON_CODE, true);
        }
        return false;
    }

    public function render_qoopon_label( $label, $coupon )
    {
        if ($coupon->get_meta(self::QOOPON_CODE, true)) {
            $frame = $this->qoopon_frame;
            $qoopon = $coupon->get_meta(self::QOOPON_META, true);
            $thumb = $qoopon['thumbnail'];
            $text = str_replace("\n\n", "\n", $coupon->get_description());
            $title = explode("\n", $text)[0];
            $limit = 40;
            return 'Qoopon - '.
                substr($title, 0, mb_strlen($title) > $limit ? $limit-3 : $limit).
                (mb_strlen($title) > $limit ? '...' : '').<<<HTML
                <div class="qoopon-coupon">
                    <div class="img" style="background-image: url($thumb);"></div>
                </div>
                <style>
                .qoopon-coupon {
                    max-width: 180px;
                    background-image: url($frame);
                    background-size: contain;
                    background-repeat: no-repeat;
                    background-position: center;
                }
                .qoopon-coupon .img {
                    background-size: 69%;
                    background-repeat: no-repeat;
                    background-position: center;
                    padding-bottom: 100%;
                }
                </style>
HTML;
        }
        return $label;
    }

    public function render_qoopon_html( $coupon_html, $coupon, $discount_amount_html )
    {
        return str_replace( $discount_amount_html, '', $coupon_html );
    }

    public function render_cart_item( $item_data, $cart_item )
    {
        if ( isset($_GET['qoopon-debug']) ) {
            if ( isset( $cart_item[self::QOOPON_CODE] ) ) {
                $item_data[] = array(
                    'key'   => 'qoopon',
                    'value' => $cart_item[self::QOOPON_CODE]['a'].' << '.$cart_item[self::QOOPON_CODE]['b'],
                );
            }
        }
        return $item_data;
    }

    public function woocommerce_cart_item_restored( $cart_item_key, $cart )
    {
        $cart_item = $cart->get_cart_item( $cart_item_key );
        if ( isset( $cart_item[self::QOOPON_CODE] ) ) {
            $coupon_code = $cart_item[self::QOOPON_CODE]['c'];
            WC()->cart->apply_coupon( $coupon_code );
        }
    }

    public function get_cart_item_from_session( $cart_item, $values )
    {
        if ( isset( $values[self::QOOPON_CODE] ) ) {
            $cart_item[self::QOOPON_CODE] = $values[self::QOOPON_CODE];
        }

        return $cart_item;
    }

    public function checkout_create_order_line_item( $item, $cart_item_key, $values, $order )
    {
        if ( isset( $values[self::QOOPON_CODE] ) ) {
            $item->add_meta_data( self::QOOPON_CODE, $values[self::QOOPON_CODE], true );
        }
    }

    public function woocommerce_coupon_error( $err, $err_code, $that )
    {
        $matches = array();
        if (preg_match('/"([^"]+)"/', $err, $matches)) {
            foreach ($matches as $i => $code)
            {
                if ($i == 0) continue;
                // if (!$this->is_qoopon($code)) continue;
                $err = str_replace('coupon ', 'Qoopon', $err);
                $err = str_replace('"'.$code.'"', '', $err);
            }
        }
        return $err;
    }

    public function get_all_discount_types( $types )
    {
        return array_merge( $types, self::get_qoopon_discount_types() );
    }

    public static function get_qoopon_discount_types()
    {
        return array(
            'bogo' => __( 'Buy 1 Get 1 Free', 'qoopon' ),
            'bogo_x' => __( 'Buy X Get 1 Free', 'qoopon' ),
            'bogo_xx' => __( 'Buy X Get % off on X+1th item', 'qoopon' ),
            'group' => __( 'Buy X for $Y', 'qoopon' ),
            'group_discount' => __( 'Buy X get $Y discount', 'qoopon' ),
            'group_discount_percent' => __( 'Buy X get Y% discount', 'qoopon' ),
        );
    }

    public static function get_qoopon_summary( $coupon, $option )
    {
        $title = '';
        $summary = '';
        $type = $coupon->get_discount_type();
        $products = $coupon->get_product_ids();
        $products_str = '';
        $all_products_str = '';
        foreach ($products as $i => $prod_id)
        {
            $prod = wc_get_product( $prod_id );
            if ($i == 0) {
                $products_str .= $prod->get_name();
            } else if ($i == 1) {
                $products_str .= __( ' (and others)', 'qoopon' );
            }
            $all_products_str .= ($i == 0 ? '' : __( ', ', 'qoopon' )).$prod->get_name();
        }
        switch ($type)
        {
            case 'percent':
                if ($products_str) {
                    $title = sprintf( __( '%d%% discount on buying %s!', 'qoopon' ),
                        $coupon->get_amount(),
                        $products_str
                    );
                } else {
                    $title = sprintf( __( '%d%% off for your purchase!', 'qoopon' ),
                        $coupon->get_amount()
                    );
                }
            break;
            case 'fixed_cart':
                $title = sprintf( __( '%s%d discount for your purchase!', 'qoopon' ),
                    html_entity_decode(get_woocommerce_currency_symbol()),
                    $coupon->get_amount()
                );
            break;
            case 'fixed_product':
                $title = sprintf( __( '%s%d discount on buying %s!', 'qoopon' ),html_entity_decode(get_woocommerce_currency_symbol()),
                    $coupon->get_amount(),
                    $products_str
                );
            break;
            case 'bogo': case 'bogo_x':
                $title = sprintf( __( 'Buy %d Get 1 Free on %s!', 'qoopon' ),
                    self::get_option_field( $option, 'bogo_purchase_quantity', 1 ),
                    $products_str
                );
            break;
            case 'bogo_xx':
                $quantity = self::get_option_field( $option, 'bogo_purchase_quantity', 1 );
                $title = sprintf( __( 'Buy %d Get %d%% off on %d%s %s!', 'qoopon' ),
                    $quantity,
                    self::get_option_field( $option, 'bogo_pricing_value', 100 ),
                    $quantity + 1,  ($quantity + 1 == 1 ? 'st' :
                                    ($quantity + 1 == 2 ? 'nd' :
                                    ($quantity + 1 == 3 ? 'rd' : 'th'))),
                    $products_str
                );
            break;
            case 'group':
                $title = sprintf( __( 'Buy %d %s for %s%d!', 'qoopon' ),
                    self::get_group_quantity( $option ),
                    $products_str,
                    html_entity_decode(get_woocommerce_currency_symbol()),
                    self::get_group_pricing( $option )
                );
            break;
            case 'group_discount':
                $title = sprintf( __( 'Buy %d %s get %s%d discount!', 'qoopon' ),
                    self::get_group_quantity( $option ),
                    $products_str,
                    html_entity_decode(get_woocommerce_currency_symbol()),
                    self::get_group_pricing( $option )
                );
            break;
            case 'group_discount_percent':
                $title = sprintf( __( 'Buy %d %s get %d%% off!', 'qoopon' ),
                    self::get_group_quantity( $option ),
                    $products_str,
                    self::get_group_pricing( $option )
                );
            break;
        }
        if ($coupon->get_date_expires()) {
            $summary .= __( 'Valid through ', 'qoopon' ).explode('T', $coupon->get_date_expires())[0].'. ';
        }
        if ($coupon->get_free_shipping()) {
            $summary .= __( 'Free shipping included. ', 'qoopon' );
        }
        if ($categories = $coupon->get_product_categories()) {
            $summary .= __( 'Only for the following product categories: ', 'qoopon' );
            foreach ($categories as $i => $cat_id)
            {
                $cat = get_term( $cat_id, 'product_cat' );
                $summary .= ($i == 0 ? '' : __( ', ', 'qoopon' )).$cat->name;
            }
            $summary .= __( '. ', 'qoopon' );
        }
        if ($ex_categories = $coupon->get_excluded_product_categories()) {
            $summary .= __( 'Excluding the following product categories: ', 'qoopon' );
            foreach ($ex_categories as $i => $cat_id)
            {
                $cat = get_term( $cat_id, 'product_cat' );
                $summary .= ($i == 0 ? '' : __( ', ', 'qoopon' )).$cat->name;
            }
            $summary .= __( '. ', 'qoopon' );
        }
        switch ($type)
        {
            case 'fixed_cart':
                if ($all_products_str) {
                    $summary .= __( 'Has to include in cart: ', 'qoopon' ).$all_products_str.'. ';
                } else {
                    $summary .= __( 'Applies to entire cart. ', 'qoopon' );
                }
            break;
            case 'percent':
                if ($all_products_str) {
                    $summary .= __( 'Applies to: ', 'qoopon' ).$all_products_str.'. ';
                } else {
                    $summary .= __( 'Applies to entire cart. ', 'qoopon' );
                }
            break;
            case 'fixed_product':
                $summary .= __( 'Applies to: ', 'qoopon' ).$all_products_str.'. ';
            break;

            case 'bogo':
            case 'bogo_x':
            case 'bogo_xx':
                if (!self::get_bogo_repeat( $option )) {
                    $summary .= __( 'Discount can be applied once only. ', 'qoopon' );
                } else {
                    $summary .= __( 'Discount can be applied multiple times. ', 'qoopon' );
                }
                if (!self::get_bogo_mix( $option )) {
                    $summary .= __( 'Applies to: ', 'qoopon' ).$all_products_str.'. ';
                } else {
                    $summary .= __( 'Can mix & match among: ', 'qoopon' ).$all_products_str.'. ';
                }
            break;

            case 'group':
            case 'group_discount':
            case 'group_discount_percent':
                if (count($products) == 1) {
                    $summary .= __( 'Applies to: ', 'qoopon' ).$all_products_str.'. ';
                } else {
                    $summary .= __( 'Can mix & match among: ', 'qoopon' ).$all_products_str.'. ';
                }
            break;
        }
        if ($coupon->get_minimum_amount()) {
            $summary .= sprintf( __( 'Minimum spend %s%d required. ', 'qoopon' ),html_entity_decode(get_woocommerce_currency_symbol()),
                $coupon->get_minimum_amount()
            );
        }
        if ($coupon->get_maximum_amount()) {
            $summary .= sprintf( __( 'On purchase up to %s%d. ', 'qoopon' ),
                html_entity_decode(get_woocommerce_currency_symbol()),
                $coupon->get_maximum_amount()
            );
        }
        if ($coupon->get_individual_use()) {
            $summary .= __( 'Cannot be used in conjunction with other coupons. ', 'qoopon' );
        }
        if ($coupon->get_exclude_sale_items()) {
            $summary .= __( 'Excluding items on sale. ', 'qoopon' );
        }
        if ($coupon->get_excluded_product_ids()) {
            $excluded_products_str = '';
            foreach ($coupon->get_excluded_product_ids() as $i => $prod_id)
            {
                $prod = wc_get_product( $prod_id );
                $excluded_products_str .= ($i == 0 ? '' : __( ', ', 'qoopon' )).$prod->get_name();
            }
            $summary .= __( 'Excluding following items: ', 'qoopon' ).$excluded_products_str.'. ';
        }
        if ($coupon->get_limit_usage_to_x_items()) {
            $summary .= sprintf( __( 'Discount applies to at most %d qualifying items. ', 'qoopon' ),
                $coupon->get_limit_usage_to_x_items()
            );
        }
        if ($coupon->get_usage_limit()) {
            $summary .= sprintf( __( 'Only applicable to the first %d customers. ', 'qoopon' ),
                $coupon->get_usage_limit()
            );
        }
        if ($coupon->get_usage_limit_per_user()) {
            $summary .= sprintf( __( 'Each customer can use for no more than %d times. ', 'qoopon' ),
                $coupon->get_usage_limit_per_user()
            );
        }
        if ($coupon->get_email_restrictions()) {
            $summary .= __( 'Limited to select customers only. ', 'qoopon' );
        }
        $summary .= __( 'Not refundable. ', 'qoopon' );
        return "$title\n\n$summary";
    }

    public function render_order_item( $formatted_meta, $item )
    {
        if (is_admin() && isset($_GET['qoopon-debug']) &&
            $meta = $item->get_meta(self::QOOPON_CODE, true)) {
            $formatted_meta[ $meta->id ] = (object) array(
                'key'           => $meta->key,
                'value'         => $meta->value,
                'display_key'   => 'Qoopon',
                'display_value' => $meta['a'] == $meta['b'] ? $meta['a'] : $meta['a'].' << '.$meta['b'],
            );
        }
        return $formatted_meta;
    }

    public function order_item_coupon_url( $url, $coupon, $order )
    {
        $qoopon = null;
        $metas = $coupon->get_meta_data();
        if ( $metas && isset( $metas[0] ) ) {
            $metas = $metas[0];
            if ( isset( $metas->value['meta_data'] ) ) {
                $metas = $metas->value['meta_data'];
                foreach( $metas as $meta )
                {
                    if ($meta->key == self::QOOPON_META) {
                        $qoopon = $meta->value;
                        break;
                    }
                }
            }
        }
        return $url.($qoopon ? '#'.$qoopon['thumbnail'] : '');
    }

    public function admin_order_totals( $order_id )
    {
        ?>
        <style>
        .wc-used-coupons .wc_coupon_list li.code a span {
            width: 100px;
            height: 100px;
        }
        ul.wc_coupon_list li.code {
            background-color: unset;
        }
        .qoopon-coupon {
            width: 120px;
            background-image: url(<?= $this->qoopon_frame ?>);
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
        }
        .qoopon-coupon .img {
            background-size: 69%;
            background-repeat: no-repeat;
            background-position: center;
            padding-bottom: 100%;
        }
        #woocommerce-order-items .wc_coupon_list li.code {
            border: none;
        }
        </style>
        <script>
        jQuery('.wc-used-coupons .wc_coupon_list li.code a').each(function(i, el){
            var $ = jQuery;
            if (!$(el).attr('href').split('#').length) {
                return;
            }
            $(el).find('span').remove();
            $(el).append(
                $('<div class="qoopon-coupon">').append(
                    $('<div class="img" style="background-image: url('+
                        ($(el).attr('href').split('#')[1])+
                        ');"></div>')
                )
            );
            $(el).attr('href', $(el).attr('href').split('#')[0]);
        });
        </script>
        <?php
    }
}
